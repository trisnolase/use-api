<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('products', [ApiProductController::class, 'index'])->name('products.index');
Route::post('products/store', [ApiProductController::class, 'store'])->name('products.store');
Route::get('products/show/{id}', [ApiProductController::class, 'show'])->name('products.show');
Route::get('products/delete/{id}', [ApiProductController::class, 'destroy'])->name('products.delete');
