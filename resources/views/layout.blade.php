<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Use API</title>
    <meta name="author" content="name">
    <meta name="description" content="description here">
    <meta name="keywords" content="keywords,here">

    <link rel="shortcut icon" href="{{ asset('image/favicon.png') }}" />

    <link rel="stylesheet" href="{{ asset('tailwindlayout/tailwind.min.css') }}" />
    <link href="{{ asset('tailwindlayout/emoji.css') }}" rel="stylesheet">

    <style>
        .table-hover tbody tr:hover {
            background-color: #d0e8ff;
            box-shadow: 0px 0px 5px rgba(125, 181, 255, 0.2);
        }
    </style>
    @stack('styles')
</head>

<body class="bg-gray-100 font-sans leading-normal tracking-normal mt-12">

    <header>
        <!--Nav-->
        <nav aria-label="menu nav" class="bg-gray-200 pt-2 md:pt-1 pb-1 px-1 mt-0 h-auto fixed w-full z-20 top-0">
            <div class="flex flex-wrap items-center">
                <div class="flex pt-2 w-1/2 justify-start text-black">
                    <a aria-label="Home">
                        <span class="text-xl pl-2"><i class="em em-100"></i></span>
                    </a>
                </div>

                <div class="flex pt-2 justify-end w-1/2">
                    <ul class="list-reset flex justify-between flex-1 flex-none items-center">
                        <li class="flex-1 flex-none mr-3">
                            <div class="relative inline-block">
                                <button onclick="toggleDD('myDropdown')" class="drop-button text-black py-2 px-2"><span class="pr-2"><i class="em em-robot_face"></i></span>Yoo, Kam </button>
                                <div id="myDropdown" class="dropdownlist absolute bg-gray-200 text-black right-0 mt-3 p-3 overflow-auto z-30 invisible">
                                    <a class="p-2 hover:bg-gray-100 text-black text-sm no-underline hover:no-underline block"> Profile</a>
                                    <a class="p-2 hover:bg-gray-100 text-black text-sm no-underline hover:no-underline block"> Settings</a>
                                    <div class="border border-gray-800"></div>
                                    <a class="p-2 hover:bg-gray-100 text-black text-sm no-underline hover:no-underline block"> Log Out</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <main>
        <div class="flex flex-col md:flex-row">
            <nav aria-label="alternative nav">
                <div class="bg-gray-200 shadow-xl h-20 fixed bottom-0 md:relative md:h-screen z-10 w-full md:w-48 content-center">
                    <div class="md:mt-12 md:w-48 md:fixed md:left-0 md:top-0 content-center md:content-start text-left justify-between">
                        <ul class="list-reset flex flex-row md:flex-col pt-3 md:py-3 px-1 md:px-2 text-center md:text-left">
                            <li class="mr-3 flex-1">
                                <a href="{{ route('dashboard') }}" class="block py-1 md:py-3 pl-1 align-middle text-black no-underline border-b-2 border-gray-300 hover:border-pink-500">
                                    <i class="em em-bar_chart pr-0 md:pr-3 md:mr-3"></i><span class="pb-1 md:pb-0 text-xs md:text-base block md:inline-block">Dashboard</span>
                                </a>
                            </li>
                            <li class="mr-3; flex-1">
                                <a class="block py-1 md:py-3 pl-1 align-middle text-black no-underline hover:text-black border-b-2 border-gray-300 hover:border-purple-500">
                                    <i class="em em-smiley pr-0 md:pr-3 md:mr-3"></i><span class="pb-1 md:pb-0 text-xs md:text-base block md:inline-block">Profile</span>
                                </a>
                            </li>
                            <li class="mr-3 flex-1">
                                <a href="{{ route('products.index') }}" class="block py-1 md:py-3 pl-1 align-middle text-black no-underline hover:text-black border-b-2 border-gray-300 hover:border-blue-600">
                                    <i class="em em-package pr-0 md:pr-3 md:mr-3"></i><span class="pb-1 md:pb-0 text-xs md:text-base block md:inline-block">Product</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <section class="w-full">
                <div id="main" class="main-content">
                    <div class="pt-2">
                        <div class="bg-gradient-to-r from-blue-300 to-pink-300 p-4 shadow text-2xl text-white">
                            <h1 class="font-bold pl-2">@yield('page-title')</h1>
                        </div>
                    </div>

                    <div>
                        @yield('content')
                    </div>
                </div>
            </section>
        </div>
    </main>

    <script>
        /*Toggle dropdown list*/
        function toggleDD(myDropMenu) {
            document.getElementById(myDropMenu).classList.toggle("invisible");
        }
        /*Filter dropdown options*/
        function filterDD(myDropMenu, myDropMenuSearch) {
            var input, filter, ul, li, a, i;
            input = document.getElementById(myDropMenuSearch);
            filter = input.value.toUpperCase();
            div = document.getElementById(myDropMenu);
            a = div.getElementsByTagName("a");
            for (i = 0; i < a.length; i++) {
                if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
                    a[i].style.display = "";
                } else {
                    a[i].style.display = "none";
                }
            }
        }
        // Close the dropdown menu if the user clicks outside of it
        window.onclick = function(event) {
            if (!event.target.matches('.drop-button') && !event.target.matches('.drop-search')) {
                var dropdowns = document.getElementsByClassName("dropdownlist");
                for (var i = 0; i < dropdowns.length; i++) {
                    var openDropdown = dropdowns[i];
                    if (!openDropdown.classList.contains('invisible')) {
                        openDropdown.classList.add('invisible');
                    }
                }
            }
        }
    </script>

</body>

</html>
