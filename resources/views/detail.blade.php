@extends('layout')

@section('page-title')
    Products Detail
@endsection

@section('content')
    <div class="mx-auto p-4">
        <div class="w-full p-6 border rounded-lg shadow-lg">
            <h1 class="text-3xl font-bold">{{ $product['name'] }}</h1>
            <p class="text-lg">{{ $product['description'] }}</p>
            <p class="text-lg">Price: ${{ number_format($product['price'], 2) }}</p>
            <p class="text-lg">Stock: {{ $product['stock'] }}</p>

            <h2 class="text-2xl mt-4">Colors:</h2>
            @foreach ($product['colors'] as $color)
                <tr>
                    <td class="px-4 py-2">{{ $color['color'] }}
                        <div style="background-color: {{ $color['color'] }}; width: 100px; height: 20px;"></div>
                    </td>
                </tr>
            @endforeach
        </div>
    </div>
@endsection
