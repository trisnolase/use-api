@extends('layout')

@section('page-title')
    Products
@endsection

@section('content')
    <div class="pt-4 pl-4 pr-4">
        <div class="flex flex-col md:flex-row">
            {{-- Form Section  --}}
            <div class="md:w-1/4">
                <div class="bg-white p-8 rounded-lg shadow-md md:mr-4 mb-4">
                    <form action="{{ route('products.store') }}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <input value="{{ isset($editProduct['id']) ? $editProduct['id'] : '' }}" type="hidden" id="id" name="id" />
                        <div class="mb-4">
                            <label for="name" class="block text-gray-600 text-sm font-medium">Name</label>
                            <input value="{{ isset($editProduct['name']) ? $editProduct['name'] : old('name') }}" type="text" id="name" name="name" class="w-full px-3 py-2 border rounded-lg focus:outline-none focus:ring focus:border-blue-300" placeholder="Product Name" />
                            @error('name')
                                <div class="text-red-500 text-xs mt-1">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-4">
                            <label for="description" class="block text-gray-600 text-sm font-medium">Description</label>
                            <textarea id="description" name="description" class="w-full px-3 py-2 border rounded-lg focus:outline-none focus:ring focus:border-blue-300" placeholder="Product Description">{{ isset($editProduct['description']) ? $editProduct['description'] : old('description') }}</textarea>
                            @error('description')
                                <div class="text-red-500 text-xs mt-1">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-4">
                            <label for="price" class="block text-gray-600 text-sm font-medium">Price</label>
                            <input value="{{ isset($editProduct['price']) ? $editProduct['price'] : old('price') }}" type="number" id="price" name="price" class="w-full px-3 py-2 border rounded-lg focus:outline-none focus:ring focus:border-blue-300" placeholder="Product Price" />
                            @error('price')
                                <div class="text-red-500 text-xs mt-1">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-4">
                            <label for="stock" class="block text-gray-600 text-sm font-medium">Stock</label>
                            <input value="{{ isset($editProduct['stock']) ? $editProduct['stock'] : old('stock') }}" type="number" id="stock" name="stock" class="w-full px-3 py-2 border rounded-lg focus:outline-none focus:ring focus:border-blue-300" placeholder="Product Stock" />
                            @error('stock')
                                <div class="text-red-500 text-xs mt-1">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mt-4">
                            <button type="submit" class="w-full bg-blue-500 text-white py-2 px-4 rounded-lg hover:bg-blue-600 focus:outline-none focus:ring focus:bg-blue-400">
                                Save
                            </button>
                        </div>
                        <div class="mt-4">
                            <button type="reset" class="w-full bg-red-500 text-white py-2 px-4 rounded-lg hover:bg-red-600 focus:outline-none focus:ring focus:bg-red-400">
                                Reset
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            {{-- Product Index Section  --}}
            <div class="md:w-3/4">
                <div class="bg-white rounded-lg shadow-md">
                    <div class="overflow-x-auto">
                        <table class="w-full table-hover">
                            <thead>
                                <tr>
                                    <th class="px-6 py-3 text-left text-xl font-semibold text-gray-900">Name</th>
                                    <th class="px-6 py-3 text-left text-xl font-semibold text-gray-900">Description</th>
                                    <th class="px-6 py-3 text-left text-xl font-semibold text-gray-900">Price</th>
                                    <th class="px-6 py-3 text-left text-xl font-semibold text-gray-900">Stock</th>
                                    <th class="px-6 py-3 text-left text-xl font-semibold text-gray-900">Avaliable Color</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!$products)
                                    <tr>
                                        <td colspan="5" class="px-6 py-3 text-left text-sm text-gray-500">No data.</td>
                                    </tr>
                                @else
                                    @foreach ($products as $product)
                                        <tr>
                                            <td class="px-6 py-3 text-left text-sm text-gray-500">{{ $product['name'] }}</td>
                                            <td class="px-6 py-3 text-left text-sm text-gray-500">{{ $product['description'] }}</td>
                                            <td class="px-6 py-3 text-left text-sm text-gray-500">{{ $product['price'] }}</td>
                                            <td class="px-6 py-3 text-left text-sm text-gray-500">{{ $product['stock'] }}</td>
                                            <td class="px-6 py-3 text-left text-sm text-gray-500">
                                                {{ implode(', ', array_column($product['colors'], 'color')) }}
                                            </td>
                                            <td class="px-6 py-3 text-left text-sm text-gray-500">
                                                <a href="{{ route('products.show', ['id' => $product['id']]) }}">Detail</a> |
                                                <a href="{{ route('products.index', ['id' => $product['id']]) }}">Edit</a> |
                                                <a href="{{ route('products.delete', ['id' => $product['id']]) }}">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
