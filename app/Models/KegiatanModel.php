<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KegiatanModel extends Model
{
    protected $table  = 'kegiatan';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;
}
