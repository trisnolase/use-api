<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class ApiProductController extends Controller
{
    public function index()
    {
        $id = request()->id;
        if ($id) $editProduct = Http::get("http://127.0.0.1:8000/api/products/$id");
        $getProductsApi = Http::get('http://127.0.0.1:8000/api/products');

        $products = $getProductsApi->json();
        if (!$getProductsApi->successful()) {
            Log::error('Error: ' . $getProductsApi->status());
            Log::error('postProductsApi: ' . $getProductsApi->body());
        }

        if ($id) return view('products', compact('products', 'editProduct'));
        if (!$id) return view('products', compact('products'));
    }

    public function store(Request $request)
    {
        $id = request()->id;

        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
        ]);

        $productData = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'price' => $request->input('price'),
            'stock' => $request->input('stock'),
        ];

        if ($id) $postProductsApi = Http::put("http://127.0.0.1:8000/api/products/$id", $productData);
        if (!$id) $postProductsApi = Http::post('http://127.0.0.1:8000/api/products', $productData);

        if ($postProductsApi->successful()) {
            return redirect()->route('products.index');
        } else {
            Log::error('Error: ' . $postProductsApi->status());
            Log::error('Response: ' . $postProductsApi->body());
        }
    }

    public function show($id)
    {
        $product = Http::get('http://127.0.0.1:8000/api/products/' . $id);

        return view('detail', compact('product'));
    }

    public function destroy($id)
    {
        $deleteProductsApi = Http::delete("http://127.0.0.1:8000/api/products/$id");

        if ($deleteProductsApi->successful()) {
            return redirect()->route('products.index');
        } else {
            Log::error('Error: ' . $deleteProductsApi->status());
            Log::error('Response: ' . $deleteProductsApi->body());
        }
    }
}
